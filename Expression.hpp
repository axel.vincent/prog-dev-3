#include <iostream>
using namespace std;

class Expression
{
public:
    Expression()
    {
        ++nb_instances_;
    };

    friend ostream &operator<<(ostream &out, const Expression &e)
    {
        return e.affiche(out);
        //return out;
    };
    virtual std::ostream &affiche(std::ostream &out) const = 0;
    virtual Expression *derive(const string n) const = 0;
    virtual Expression *clone() const = 0;

    virtual Expression *simplify() const = 0;

    virtual ~Expression()
    {
        --nb_instances_;
    };

    static int nb_instances()
    {
        return nb_instances_;
    }

private:
    static int nb_instances_;
};

int Expression::nb_instances_{0};

class Nombre : public Expression
{
public:
    Nombre()
    {
        value = 0;
    };
    Nombre(const long int &n)
    {
        value = n;
    };

    std::ostream &affiche(std::ostream &out) const override
    {
        //cout << "salut";
        //out << value;
        return out << value;
    };

    Expression *derive(const string n) const override
    {
        return new Nombre(0);
    };
    Expression *clone() const override
    {
        return new Nombre(value);
    };

    Expression *simplify() const override
    {
        return this->clone();
    }
    friend ostream &operator<<(ostream &out, const Nombre &n)
    {
        out << n.value;
        return out;
    };

    ~Nombre(){};

private:
    int value;
};

class Variable : public Expression
{
public:
    Variable()
    {
        name = "";
    };
    Variable(const string &n)
    {
        name = n;
    };

    std::ostream &affiche(std::ostream &out) const override
    {
        //out << name;
        return out << name;
    };

    Expression *derive(const string n) const override
    {
        if (name == n)
        {
            return new Nombre(1);
        }
        return new Nombre(0);
    };

    Expression *clone() const override
    {
        return new Variable(name);
    };

    Expression *simplify() const override
    {
        return (*this).clone();
    }

    // friend ostream &operator<<(ostream &out, const Variable &x)
    // {
    //     out << x.name;
    //     return out;
    // };

    ~Variable(){};

    string name;
};

class Operation : public Expression
{
public:
    Operation(){};

    ~Operation(){};

protected:
    Expression *expr1;
    Expression *expr2;
};

class Addition : public Operation
{
public:
    Addition(Expression *exp1, Expression *exp2)
    {
        expr1 = exp1;
        expr2 = exp2;
    };
    std::ostream &affiche(std::ostream &out) const override
    {
        return out << "(" << *expr1 << " + " << *expr2 << (")");
    };

    Expression *derive(const string n) const override
    {
        Expression *e1 = (*expr1).derive(n);
        Expression *e2 = (*expr2).derive(n);

        return new Addition(e1, e2);
    };

    Expression *clone() const override
    {
        return new Addition(expr1, expr2);
    };

    Expression *simplify() const override
    {
        std::ostringstream os;
        os << *expr1;
        if (os.str() == "0")
        {
            return (*expr2).simplify();
        }

        std::ostringstream os2;
        os2 << *expr2;
        if (os2.str() == "0")
        {
            return (*expr1).simplify();
        }
        return (*this).clone();
    }

    ~Addition(){};
};

class Multiplication : public Operation
{
public:
    Multiplication(Expression *exp1, Expression *exp2)
    {
        expr1 = exp1;
        expr2 = exp2;
    };
    std::ostream &affiche(std::ostream &out) const override
    {
        return out << "(" << *expr1 << " * " << *expr2 << (")");
    };

    Expression *derive(const string n) const override
    {
        Expression *e1 = (*expr1).clone();
        Expression *e2 = (*expr2).clone();

        Expression *eDer1 = (*expr1).derive(n);
        Expression *eDer2 = (*expr2).derive(n);

        Expression *left = new Multiplication(eDer1, e2);
        Expression *right = new Multiplication(e1, eDer2);

        return new Addition(left, right);
    };

    Expression *clone() const override
    {
        return new Multiplication(expr1, expr2);
    };

    Expression *simplify() const override
    {
        std::ostringstream os;
        os << *expr1;
        if (os.str() == "0")
        {
            return new Nombre(0);
        }
        return (*this).clone();
    }

    ~Multiplication(){};
};